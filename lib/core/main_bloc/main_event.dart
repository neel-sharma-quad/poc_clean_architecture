part of 'main_bloc.dart';

abstract class MainEvent extends Equatable {
  const MainEvent();
}

class AppStarted extends MainEvent {
  @override
  List<Object> get props => [];
}

class LoggedIn extends MainEvent {
  final String token;

  const LoggedIn({
    required this.token,
  });

  @override
  List<Object> get props => [
        token,
      ];
}

class LoggedOut extends MainEvent {
  @override
  List<Object> get props => [];
}

class GetProfileEvent extends MainEvent {
  final String id;

  const GetProfileEvent({required this.id});

  @override
  List<Object?> get props => [id];
}

class ToggleThemeEvent extends MainEvent {
  @override
  List<Object> get props => [];
}

class SetSystemDefaultThemeEvent extends MainEvent {
  @override
  List<Object> get props => [];
}

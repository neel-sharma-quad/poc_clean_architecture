part of 'main_bloc.dart';

abstract class MainState extends Equatable {
  const MainState();
}

class MainInitial extends MainState {
  @override
  List<Object> get props => [];
}

class MainLoadingState extends MainState {
  @override
  List<Object> get props => [];
}

class MainErrorState extends MainState {
  final String message;

  const MainErrorState({required this.message});

  @override
  List<Object> get props => [message];
}

class Uninitialized extends MainState {
  @override
  List<Object?> get props => [];
}

class Authenticated extends MainState {
  final bool isLoggedIn;

  const Authenticated({
    required this.isLoggedIn,
  });

  @override
  List<Object?> get props => [];
}

class Unauthenticated extends MainState {
  const Unauthenticated();

  @override
  List<Object?> get props => [];
}

class GetProfileLoadedState extends MainState {
  final String? successMessage;

  const GetProfileLoadedState({this.successMessage});

  @override
  List<Object?> get props => [successMessage];
}

class LightThemeToggleState extends MainState {
  @override
  List<Object> get props => [];
}

class DarkThemeToggleState extends MainState {
  @override
  List<Object> get props => [];
}

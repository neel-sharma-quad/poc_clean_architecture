import 'package:poc_clean_architecture/core/theme/app_config.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:poc_clean_architecture/core/error/failures.dart';
import 'package:poc_clean_architecture/features/data/datasources/local_datasource/local_datasource.dart';
import 'package:poc_clean_architecture/features/domain/usecases/get_profile_use_case.dart';


part 'main_event.dart';

part 'main_state.dart';

class MainBloc extends Bloc<MainEvent, MainState> {
  final GetProfileUseCase _getProfileUseCase;
  final LocalDatasource _localDataSource;

  MainBloc({
    required final GetProfileUseCase getProfileUseCase,
    required final LocalDatasource localDataSource,
  })
      : _getProfileUseCase = getProfileUseCase,
        _localDataSource = localDataSource,
        super(
        // MainInitial(),
        Uninitialized(),
      ) {
    on<MainEvent>((event, emit) async {
      if (event is AppStarted) {
        if (true) {
          emit.call(
            const Authenticated(
              isLoggedIn: true,
            ),
          );
        } else {
          emit.call(
            const Unauthenticated(),
          );
        }
      } else if (event is LoggedIn) {
        emit.call(
          const Authenticated(
            isLoggedIn: true,
          ),
        );
      } else if (event is LoggedOut) {

        emit.call(const Unauthenticated());
      } else if (event is GetProfileEvent) {
        emit.call(MainLoadingState());
        final data = await _getProfileUseCase.call(
          GetProfileParams(id: event.id),
        );
        data.fold((failure) async {
          if (failure is CacheFailure) {
            emit.call(MainErrorState(message: failure.message));
          } else if (failure is ServerFailure) {
            emit.call(MainErrorState(message: failure.message));
          } else {
            emit.call(
              const MainErrorState(message: 'Unknown Error'),
            );
          }
        }, (loadedDataEntity) async {
          emit.call(
            GetProfileLoadedState(
              successMessage: loadedDataEntity,
            ),
          );
        });
      }

    });
  }
}


class Constants {
  static const String appName = "POC Clean Architecture";

  static const String authorization = "Authorization";

  static const tempNetworkUrl =
      "https://images.unsplash.com/photo-1604426633861-11b2faead63c?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1900&q=80";

  static const errorInternet = "Your internet is not working it seems";
  static const String errorNoInternet = "No internet available";
  static const String errorNoDataFound = "No Data Found";
  static const String errorUnknown = "Unknown error occurred";
  static const String errorTypeServer = "Server Error";
  static const String errorTypeTimeout = "Time Out";

  static const String platformAndroid = "android";
  static const String platformIos = "ios";

  static const String baseUrl = "https://www.thecocktaildb.com/api/json/";
  static const String apiVersion = "/v1";
  static const String searchDrinkEndPoint = "/1/search.php";

}

class DBConstants {
  static const String userLocalDB = "user_table";
  static const String rolesLocalDB = "roles_table";
}

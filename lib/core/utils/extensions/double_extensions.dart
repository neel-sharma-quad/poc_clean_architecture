extension DoubleExtensions on double {
  bool get isInteger => (this % 1) == 0;

}



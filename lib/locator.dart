import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:poc_clean_architecture/core/config/db_provider.dart';
import 'package:poc_clean_architecture/core/config/my_shared_pref.dart';
import 'package:poc_clean_architecture/core/main_bloc/main_bloc.dart';
import 'package:poc_clean_architecture/core/network/network_info.dart';
import 'package:poc_clean_architecture/core/utils/constants.dart';
import 'package:poc_clean_architecture/features/data/clients/rest_client.dart';
import 'package:poc_clean_architecture/features/data/datasources/local_datasource/local_datasource.dart';
import 'package:poc_clean_architecture/features/data/datasources/remote_datasource/remote_datasource.dart';
import 'package:poc_clean_architecture/features/data/repositories_implementation/repository_implementation.dart';
import 'package:poc_clean_architecture/features/domain/repositories/repository.dart';
import 'package:poc_clean_architecture/features/domain/usecases/get_profile_use_case.dart';
import 'package:poc_clean_architecture/features/domain/usecases/search_drink_use_case.dart';
import 'package:poc_clean_architecture/features/presentation/drink_listing_screen/drink_listing_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

GetIt locator = GetIt.instance;

Future setupLocator() async {
  locator.registerFactory(
    () => MainBloc(
      localDataSource: locator(),
      getProfileUseCase: locator(),
    ),
  );

  locator.registerFactory(
    () => DrinkListingBloc(
      searchDrinkUseCase: locator(),
    ),
  );

  /// UseCases
  locator.registerLazySingleton(() => SearchDrinkUseCase(locator()));
  locator.registerLazySingleton(() => GetProfileUseCase());

  /// DataSource
  locator.registerLazySingleton<LocalDatasource>(
    () => LocalDatasourceImplementation(
      mySharedPref: locator(),
      dbProvider: locator(),
    ),
  );

  locator.registerLazySingleton<RemoteDatasource>(
    () => RemoteDatasourceImplementation(
      client: locator(),
    ),
  );

  locator.registerLazySingleton<NetworkInfo>(
      () => NetworkInfoImplementation(locator()));

  locator.registerLazySingleton<MySharedPref>(() => MySharedPref(locator()));

  ///Repository
  locator.registerLazySingleton<Repository>(
    () => RepositoryImplementation(
      localDataSource: locator(),
      remoteDataSource: locator(),
      networkInfo: locator(),
    ),
  );

  /// Initializing DIO
  final dio = Dio(
    BaseOptions(
      baseUrl: Constants.baseUrl,
    ),
  );
  dio.interceptors.add(
    LogInterceptor(
      request: true,
      responseHeader: true,
      responseBody: true,
      requestBody: true,
      requestHeader: true,
      error: true,
    ),
  );

  ///Others

  final dbProvider = DBProvider();
  final sharedPreferences = await SharedPreferences.getInstance();

  locator.registerLazySingleton<SharedPreferences>(() => sharedPreferences);
  locator.registerLazySingleton<DBProvider>(() => dbProvider);
  locator.registerLazySingleton(() => RestClient(
        dio,
        locator(),
      ));
  locator.registerLazySingleton(() => InternetConnectionChecker());
}

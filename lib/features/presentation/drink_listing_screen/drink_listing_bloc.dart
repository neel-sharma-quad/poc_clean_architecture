import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:poc_clean_architecture/core/error/failures.dart';
import 'package:poc_clean_architecture/core/utils/constants.dart';
import 'package:poc_clean_architecture/features/domain/entities/drink_listing_entity.dart';
import 'package:poc_clean_architecture/features/domain/usecases/search_drink_use_case.dart';

part 'drink_listing_event.dart';

part 'drink_listing_state.dart';

class DrinkListingBloc extends Bloc<DrinkListingEvent, DrinkListingState> {
  final SearchDrinkUseCase _searchDrinkUseCase;

  DrinkListingBloc({
    required final SearchDrinkUseCase searchDrinkUseCase,
  })  : _searchDrinkUseCase = searchDrinkUseCase,
        super(DrinkListingInitial()) {
    on<DrinkListingEvent>((event, emit) async {
      if (event is SearchDrinkEvent) {
        emit.call(DrinkLoadingState());
        final data = await _searchDrinkUseCase.call(
          SearchDrinkParams(searchText: event.searchText),
        );
        data.fold((failure) async {
          if (failure is CacheFailure) {
            emit.call(DrinkErrorState(message: failure.message));
          } else if (failure is ServerFailure) {
            emit.call(DrinkErrorState(message: failure.message));
          } else {
            emit.call(
                const DrinkErrorState(message: Constants.errorNoInternet));
          }
        }, (loadedList) async {
          emit.call(
            DrinkLoadedState(drinkList: loadedList),
          );
        });
      }
    });
  }
}

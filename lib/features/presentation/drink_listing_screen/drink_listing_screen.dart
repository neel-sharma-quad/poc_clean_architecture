import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:poc_clean_architecture/core/utils/constants.dart';
import 'package:poc_clean_architecture/core/utils/debouncer.dart';
import 'package:poc_clean_architecture/core/utils/extensions/context_extensions.dart';
import 'package:poc_clean_architecture/features/domain/entities/drink_listing_entity.dart';
import 'package:poc_clean_architecture/features/presentation/drink_listing_screen/drink_listing_bloc.dart';

class DrinkListingScreen extends StatefulWidget {
  const DrinkListingScreen({Key? key}) : super(key: key);

  @override
  State<DrinkListingScreen> createState() => _DrinkListingScreenState();
}

class _DrinkListingScreenState extends State<DrinkListingScreen> {
  late ValueNotifier<List<DrinkListingEntity>> _drinkList;
  late ValueNotifier<bool> _showLoader;
  late ValueNotifier<bool> _showClearIcon;
  late TextEditingController _searchController;
  final Debouncer _debounce = Debouncer(milliseconds: 500);

  @override
  void initState() {
    super.initState();
    _drinkList = ValueNotifier<List<DrinkListingEntity>>([]);
    _showLoader = ValueNotifier<bool>(false);
    _showClearIcon = ValueNotifier<bool>(false);
    _searchController = TextEditingController();
    context
        .read<DrinkListingBloc>()
        .add(const SearchDrinkEvent(searchText: ''));
  }

  @override
  void dispose() {
    _drinkList.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<DrinkListingBloc, DrinkListingState>(
      listener: (BuildContext context, state) {
        if (state is DrinkErrorState) {
          _showLoader.value = false;
          context.showToast('Error Fetching Drinks');
        } else if (state is DrinkLoadingState) {
          _showLoader.value = true;
        } else if (state is DrinkLoadedState) {
          _showLoader.value = false;
          if (state.drinkList.isNotEmpty) {
            _drinkList.value = state.drinkList;
          }
        }
      },
      child: Scaffold(
        appBar: AppBar(
          title: Container(
            width: double.infinity,
            height: 40,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(5),
            ),
            child: Center(
              child: TextField(
                controller: _searchController,
                onChanged: (String? value) {
                  if (value != null && value.isNotEmpty) {
                    _showClearIcon.value = true;
                    _debounce.run(() {
                      context.read<DrinkListingBloc>().add(
                            SearchDrinkEvent(searchText: value),
                          );
                    });
                  } else {
                    _showClearIcon.value = false;
                  }
                },
                decoration: InputDecoration(
                  contentPadding: const EdgeInsets.only(top: 5),
                  prefixIcon: const Icon(Icons.search),
                  suffixIcon: ValueListenableBuilder(
                    valueListenable: _showClearIcon,
                    builder: (
                      context,
                      bool showIcon,
                      Widget? notifierWidget,
                    ) {
                      return showIcon
                          ? IconButton(
                              icon: const Icon(Icons.clear),
                              onPressed: () {
                                _searchController.clear();
                              },
                            )
                          : const SizedBox();
                    },
                  ),
                  hintText: Constants.appName,
                  border: InputBorder.none,
                ),
              ),
            ),
          ),
        ),
        body: BlocListener<DrinkListingBloc, DrinkListingState>(
          listener: (BuildContext context, state) {
            if (state is DrinkErrorState) {
              _showLoader.value = false;
              context.showToast('Error Fetching Drinks');
            } else if (state is DrinkLoadingState) {
              _showLoader.value = true;
            } else if (state is DrinkLoadedState) {
              _showLoader.value = false;
              if (state.drinkList.isNotEmpty) {
                _drinkList.value = state.drinkList;
              }
            }
          },
          child: ValueListenableBuilder(
            valueListenable: _showLoader,
            builder: (
              context,
              bool showLoader,
              Widget? notifierWidget,
            ) {
              return showLoader
                  ? const Center(
                      child: Text('Loading...'),
                    )
                  : Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        ValueListenableBuilder(
                          valueListenable: _drinkList,
                          builder: (
                            context,
                            List<DrinkListingEntity> list,
                            Widget? notifierWidget,
                          ) {
                            return Expanded(
                              child: ListView.separated(
                                padding: const EdgeInsets.only(
                                  bottom: 50,
                                  top: 10,
                                  left: 10,
                                  right: 10,
                                ),
                                physics: const AlwaysScrollableScrollPhysics(),
                                shrinkWrap: true,
                                itemBuilder: (BuildContext context, int index) {
                                  return ListTile(
                                    leading: CircleAvatar(
                                      radius: 20,
                                      child:
                                          Image.network(list[index].url ?? ''),
                                    ),
                                    title: Text(list[index].name ?? ''),
                                    subtitle:
                                        Text(list[index].description ?? ''),
                                  );
                                },
                                separatorBuilder:
                                    (BuildContext context, int index) {
                                  return const SizedBox(height: 30);
                                },
                                itemCount: list.length,
                              ),
                            );
                          },
                        ),
                      ],
                    );
            },
          ),
        ),
      ),
    );
  }
}

import 'package:dio/dio.dart';
import 'package:poc_clean_architecture/core/error/exceptions.dart';
import 'package:poc_clean_architecture/core/utils/constants.dart';
import 'package:poc_clean_architecture/core/utils/custom_extension.dart';
import 'package:poc_clean_architecture/features/data/clients/rest_client.dart';
import 'package:poc_clean_architecture/features/data/model/responses/drink_listing_response_model.dart';

/// Abstract class for loading/getting data from User Service
abstract class RemoteDatasource {

  Future<DrinkListingResponseModel> searchDrink({
    required final String searchText,
  });
}

/// Implementation class used for loading/getting data from User Service
class RemoteDatasourceImplementation extends RemoteDatasource {
  final RestClient client;

  RemoteDatasourceImplementation({required this.client});

  void _processDio(err) {
    if (err is DioError) {
      throw ServerException(
        message: err.getErrorFromDio(),
      );
    } else {
      throw ServerException(message: Constants.errorUnknown);
    }
  }

  @override
  Future<DrinkListingResponseModel> searchDrink(
      {required String searchText}) async {
    try {
      return client
          .searchDrink(searchText)
          .catchError((err) {
        _processDio(err);
      });
    } on DioError catch (e) {
      throw ServerException(message: e.getErrorFromDio());
    }
  }
}

import 'package:poc_clean_architecture/core/config/db_provider.dart';
import 'package:poc_clean_architecture/core/config/my_shared_pref.dart';

/// Abstract class for saving/loading data from local storage
abstract class LocalDatasource {
  List getListing();

  void saveListing(List list);
}

/// Implementation class used for saving/loading data from Local storage
class LocalDatasourceImplementation extends LocalDatasource {
  final MySharedPref mySharedPref;
  final DBProvider dbProvider;

  LocalDatasourceImplementation({
    required this.mySharedPref,
    required this.dbProvider,
  });

  @override
  void saveListing(List list) {}

  @override
  List getListing() {
    return [];
  }
}

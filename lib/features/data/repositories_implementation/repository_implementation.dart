import 'package:dartz/dartz.dart';
import 'package:poc_clean_architecture/core/error/exceptions.dart';
import 'package:poc_clean_architecture/core/error/failures.dart';
import 'package:poc_clean_architecture/core/network/network_info.dart';
import 'package:poc_clean_architecture/core/utils/constants.dart';
import 'package:poc_clean_architecture/features/data/datasources/local_datasource/local_datasource.dart';
import 'package:poc_clean_architecture/features/data/datasources/remote_datasource/remote_datasource.dart';
import 'package:poc_clean_architecture/features/data/model/responses/drink_listing_response_model.dart';
import 'package:poc_clean_architecture/features/domain/entities/drink_listing_entity.dart';
import 'package:poc_clean_architecture/features/domain/repositories/repository.dart';

class RepositoryImplementation extends Repository {
  final LocalDatasource localDataSource;
  final RemoteDatasource remoteDataSource;
  final NetworkInfo networkInfo;

  RepositoryImplementation({
    required this.localDataSource,
    required this.remoteDataSource,
    required this.networkInfo,
  });

  @override
  Future<Either<Failure, List<DrinkListingEntity>>> searchDrink({
    required final String searchText,
  }) async {
    try {
      if (await networkInfo.isConnected) {
        final DrinkListingResponseModel result =
            await remoteDataSource.searchDrink(searchText: searchText);
        if (result.drinks != null) {
          return Right(_convertDrinkList(result));
        }
        return Left(ServerFailure(message: Constants.errorNoDataFound));
      } else {
        return Left(ServerFailure(message: Constants.errorNoInternet));
      }
    } on ServerException catch (e) {
      return Left(ServerFailure(message: e.message));
    }
  }

  List<DrinkListingEntity> _convertDrinkList(DrinkListingResponseModel model) {
    final List<DrinkListingEntity> drinkList = List.empty(growable: true);

    if (model.drinks != null && model.drinks!.isNotEmpty) {
      for (var i in model.drinks!) {
        drinkList.add(DrinkListingEntity(
          id: i.idDrink,
          name: i.strDrink,
          url: i.strDrinkThumb,
          description: i.strInstructions,
        ));
      }
    }

    return drinkList;
  }
}

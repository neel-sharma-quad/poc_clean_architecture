import 'package:dio/dio.dart';
import 'package:poc_clean_architecture/core/utils/constants.dart';
import 'package:poc_clean_architecture/features/data/datasources/local_datasource/local_datasource.dart';
import 'package:poc_clean_architecture/features/data/model/responses/drink_listing_response_model.dart';
import 'package:retrofit/http.dart';
part 'rest_client.g.dart';

/// Remote client for interacting with remote server
@RestApi(baseUrl: Constants.baseUrl + Constants.apiVersion)
abstract class RestClient {
  /// flutter pub run build_runner build --delete-conflicting-outputs
  factory RestClient(final Dio dio, final LocalDatasource localDataSource) {
    dio.interceptors.add(
      InterceptorsWrapper(
        onRequest: (options, handler) async {
          // Do something before request is sent
          // final accessToken = localDataSource.getAccessToken();
          // debugPrint(accessToken);
          // if (accessToken.isNotEmpty) {
          //   options.headers[Constants.authorization] =
          //       "Bearer ${Constants.gpt3TurboAccessToken}";
          // }
          return handler.next(options);
        },
        onResponse: (response, handler) {
          return handler.next(response); // continue
        },
        onError: (DioError e, handler) {
          return handler.next(e); //continue
        },
      ),
    );
    return _RestClient(dio);
  }

  @POST(Constants.searchDrinkEndPoint)
  Future<DrinkListingResponseModel> searchDrink(
    @Query("s") final String searchText,
  );
}

import 'package:json_annotation/json_annotation.dart';

part 'drink_listing_response_model.g.dart';

@JsonSerializable()
class DrinkListingResponseModel {
  DrinkListingResponseModel({
    this.drinks,
  });

  List<DrinkModel>? drinks;

  factory DrinkListingResponseModel.fromJson(Map<String, dynamic> json) =>
      _$DrinkListingResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$DrinkListingResponseModelToJson(this);
}

@JsonSerializable()
class DrinkModel {
  DrinkModel({
    required this.idDrink,
    this.strDrink,
    this.strDrinkThumb,
    this.strInstructions,
  });

  String idDrink;
  String? strDrink;
  String? strDrinkThumb;
  String? strInstructions;


  factory DrinkModel.fromJson(Map<String, dynamic> json) =>
      _$DrinkModelFromJson(json);

  Map<String, dynamic> toJson() => _$DrinkModelToJson(this);
}






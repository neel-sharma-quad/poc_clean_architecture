import 'package:dartz/dartz.dart';
import 'package:poc_clean_architecture/core/error/failures.dart';
import 'package:poc_clean_architecture/features/domain/entities/drink_listing_entity.dart';

abstract class Repository {
  Future<Either<Failure, List<DrinkListingEntity>>> searchDrink({
    required final String searchText,
  });
}

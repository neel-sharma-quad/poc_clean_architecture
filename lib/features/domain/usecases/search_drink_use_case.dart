import 'package:dartz/dartz.dart';
import 'package:poc_clean_architecture/core/error/failures.dart';
import 'package:poc_clean_architecture/core/usecase/usecase.dart';
import 'package:poc_clean_architecture/features/domain/entities/drink_listing_entity.dart';
import 'package:poc_clean_architecture/features/domain/repositories/repository.dart';

class SearchDrinkUseCase
    extends UseCase<List<DrinkListingEntity>, SearchDrinkParams> {
  final Repository _repository;

  SearchDrinkUseCase(this._repository);

  @override
  Future<Either<Failure, List<DrinkListingEntity>>> call(
      SearchDrinkParams params) async {
    return _repository.searchDrink(searchText: params.searchText);
  }
}

class SearchDrinkParams {
  final String searchText;

  SearchDrinkParams({required this.searchText});
}

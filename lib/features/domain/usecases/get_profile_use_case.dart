import 'package:dartz/dartz.dart';
import 'package:poc_clean_architecture/core/error/failures.dart';
import 'package:poc_clean_architecture/core/usecase/usecase.dart';

class GetProfileUseCase
    extends UseCase<String, GetProfileParams> {
  //final Repository _repository;

  GetProfileUseCase(
      //this._repository
      );

  @override
  Future<Either<Failure, String>> call(
      GetProfileParams params) async {
    return await Future.value(const Right('Neel Sharma'));
  }
}

class GetProfileParams {
  final String id;

  GetProfileParams({required this.id});
}
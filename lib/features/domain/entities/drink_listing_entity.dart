class DrinkListingEntity {
  DrinkListingEntity({
    required this.id,
    this.name,
    this.url,
    this.description,
  });

  String id;
  String? name;
  String? url;
  String? description;
}

// import 'package:poc_clean_architecture/core/main_bloc/main_bloc.dart';
// import 'package:poc_clean_architecture/core/theme/app_config.dart';
// import 'package:poc_clean_architecture/core/utils/app_localizations.dart';
// import 'package:poc_clean_architecture/core/utils/routes.dart';
//
// import 'package:firebase_core/firebase_core.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:flutter_easyloading/flutter_easyloading.dart';
// import 'package:path_provider/path_provider.dart' as path_provider_app;
// import 'package:poc_clean_architecture/features/presentation/drink_listing_screen/drink_listing_bloc.dart';
// import 'package:poc_clean_architecture/features/presentation/drink_listing_screen/drink_listing_screen.dart';
// import 'package:poc_clean_architecture/locator.dart';
//
// import 'package:poc_clean_architecture/core/utils/app_colors.dart';
// import 'package:poc_clean_architecture/core/utils/constants.dart';
// import 'package:firebase_core/firebase_core.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:hive_flutter/hive_flutter.dart';
// import 'package:intl/date_symbol_data_local.dart';
//
// Future<void> main() async {
//   WidgetsFlutterBinding.ensureInitialized();
//
//   await SystemChrome.setPreferredOrientations(
//     [DeviceOrientation.portraitUp],
//   ); // To turn off landscape mode
//   // await Firebase.initializeApp(
//   //   name: Constants.appName,
//   // );
//
//   SystemChrome.setSystemUIOverlayStyle(
//     const SystemUiOverlayStyle(
//       statusBarIconBrightness: Brightness.light,
//
//       /// this will change the brightness of the icons
//       statusBarColor: AppColors.transparent,
//
//       /// or any color you want
//       systemNavigationBarIconBrightness:
//       Brightness.light, //navigation bar icons' color
//     ),
//   );
//   // Plugin must be initialized before using
//   // await FlutterDownloader.initialize(
//   //   debug: true,
//   //
//   //   /// optional: set to false to disable printing logs to console (default: true)
//   //   ignoreSsl: true,
//   //
//   //   /// option: set to false to disable working with http links (default: false)
//   // );
//
//   /// Initializing Hive
//   final directory = await path_provider_app.getApplicationDocumentsDirectory();
//   Hive.init(directory.path);
//
//   await setupLocator();
//
//   // di.sl<AppConfig>().isDarkMode = di.sl<AppConfig>().isSystemDarkMode();
//   initializeDateFormatting().then((_) => runApp(const MyApp()));
//   // runApp(
//   //   const MyApp(),
//   // );
// }
//
// class MyApp extends StatefulWidget {
//   const MyApp({Key? key}) : super(key: key);
//
//   @override
//   State<MyApp> createState() => _MyAppState();
// }
//
// class _MyAppState extends State<MyApp> {
//   /// This widget is the root of your application.
//   /// Try running your application with "flutter run".
//
//   // @override
//   // void initState() {
//   //   final window = WidgetsBinding.instance.window;
//   //   window.onPlatformBrightnessChanged = () {
//   //     context.read<MainBloc>().add(SetSystemDefaultThemeEvent());
//   //   };
//   //
//   //   super.initState();
//   // }
//
//   @override
//   Widget build(BuildContext context) {
//     return BlocProvider<MainBloc>(
//       create: (context) => locator<MainBloc>()..add(AppStarted()),
//       child: BlocConsumer<MainBloc, MainState>(
//         listener: (context, state) {
//           // add broadcast listener
//         },
//         builder: (context, state) {
//           return MaterialApp(
//             title: Constants.appName,
//
//             ///locale: Locale('en', 'IND'),
//             locale: const Locale('en'),
//             builder: EasyLoading.init(),
//             // localizationsDelegates: const [
//             //   AppLocalizationsDelegate(),
//             // ],
//             // builder: DevicePreview.appBuilder,
//             supportedLocales: const [Locale('en')],
//             debugShowCheckedModeBanner: false,
//             initialRoute: AppRoutes.drinkListingScreen,
//             routes: _registerRoutes(),
//           );
//         },
//       ),
//     );
//   }
//
//   /// All the routes for this Application is defined here
//   Map<String, WidgetBuilder> _registerRoutes() {
//     return {
//       AppRoutes.drinkListingScreen: (context) => BlocProvider<DrinkListingBloc>(
//         create: (context) => locator<DrinkListingBloc>(),
//         child: const DrinkListingScreen(),
//       ),
//     };
//   }
// }
//
